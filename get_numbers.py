import re
import json
def get_numbers(d,s):
	b = s.split(',')
	b = [item.strip() for item in b]
	

	try:
		if type(int(b[0])) is int:
			if 'integers' in d:
				for i in b:
					d["integers"].append(int(i))
			else:
				d["integers"]=[int(a) for a in b]

	except:
		try:
			if type(float(b[0])) is float:
				if 'floats' in d:
					for i in b:
						d["floats"].extend(float(i) for i in b)
				else:
					d["floats"]=[float(a) for a in b]  

		except:
				if type(complex(b[0])) is complex:
					if 'complexes' in d:
						for i in b:
							d["complexes"].extend(complex(a) for a in b)
					else:
						d["complexes"] =[complex(a) for a in b]
	return d


d = get_numbers({}, '12, 34, 56')
d = get_numbers(d, '1+3j')
d = get_numbers(d, '7, 8, 32, 56')
d = get_numbers(d, '12.0, 56.34')
d = get_numbers(d, '45+9j, 3+5j, 23+j')
d = get_numbers(d, ' 23.5, 78.3')
print(d)
	#print(json.dumps(d, indent=4));

def number_exists(d,number):
	c = number
	#print(type(c))



	if c in d['integers']:
		if type(c) == int:
			print('number is exists ',c)
		else:
			if c in d['floats']:
				print("number is exists",c)
	elif c in d['complexes']:
		print('number is exists',c)

	else:
		print('number does not exists',c)

number_exists(d, 8)      
number_exists(d, 11.11)  
number_exists(d, 12.0)  
number_exists(d, (3+5j))  
#print(d)
#print(json.dumps(d, indent=4));