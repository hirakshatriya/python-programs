import pprint
def list_tuples(l):
    d = dict(l)
    print (d)
    
list_tuples( [('A', 45), ('B', 67), ('C', 9)])

def get_numbers(d,s):
  l = s.split(',')
  print l
  try:
    if type(int(l[0])) is int:
      if 'integers' in d:
        for item in l:
          d["integers"].append(int(item))
      else:
        d["integers"]=[int(a) for a in l]
  except:  
    try:    
      if type(float(l[0])) is float:
        if'floats' in d:
          d["floats"].extend([float(a) for a in l])
        else:
          d["floats"]=[float(a) for a in l]  
    except:
      if type(complex(l[0])) is complex:
        if 'complexes' in d:  
          d["complexes"] = d["complexes"]+[complex(a) for a in l]
        else:
          d["complexes"] =[complex(a) for a in l]

  return d

d = get_numbers({}, '12, 34, 56')
d = get_numbers(d, '1+3j')
d = get_numbers(d, '7, 8, 32, 56')
d = get_numbers(d, '12.0, 56.34')
d = get_numbers(d, '45+9j, 3+5j, 23+j')
d = get_numbers(d, ' 23.5, 78.3')

pprint.pprint(d)