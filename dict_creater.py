def dict_creator(d,param):
    #print ('\n lists \n')
    #d.update(param)
    if type(param) == list:
        d['lists'].append(param);
    elif type(param) == tuple:
        d['tuples'].append(param);
    elif type(param)== dict:
        d['dictionaries'].append(param);
    print (d)
   
dict_creator({'tuples':[(12,34),(1,2,3)],'lists':[[1,2],[3,4,5]],'dictionaries':
              [{23:'tiger'},{24:'complex number'}]},[6,7,8])
dict_creator({'tuples':[(12,34),(1,2,3)],'lists':[[1,2],[3,4,5]],'dictionaries'
              :[{23:'tiger'},{24:'complex number'}]},(6,7,8))
dict_creator({'tuples':[(12,34),(1,2,3)],'lists':[[1,2],[3,4,5]],'dictionaries'
              :[{23:'tiger'},{24:'complex number'}]},{25:'cat'})

