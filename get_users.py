import json

def get_users(d, s):

    arr=s.split(",")
    arr = [item.strip() for item in arr]
    
    branch = arr[0]
  
    fullname  = arr[1]
    a = arr[2]
    age = int(a)
    d2={}
 
    if branch in d:
        d[branch].append({"Fullname":fullname,"age":age})
    else:
        d[branch] = [{"Fullname":fullname,"age":age}]

    return d
    
def start():
    d = get_users({}, '       CSE        ,      Gudal Sharma,22')
    # print(json.dumps(d, indent=4));
    # print('\n-------------------------------------------------\n')

    d = get_users(d, 'IT,Bhrati Kumari        ,23')
    # print(json.dumps(d, indent=4));
    # print('\n-------------------------------------------------\n')

    d = get_users(d, 'CSE,Niyati,22')
    # print(json.dumps(d, indent=4));
    # print('\n-------------------------------------------------\n')

    d = get_users(d, 'MECH,Prince Harry,22')
    # print(json.dumps(d, indent=4));
    # print('\n-------------------------------------------------\n')

    d = get_users(d, '    CIVIL,Final Singh,         21')
    print(d)
    #print(json.dumps(d, indent=4));
    

# Starting point
start();

"""
C:\\Users\\Sjain Ventures\\Desktop\\Hira>python get_users.py
{
    "CSE": [
        {
            "Fullname": " Gudal Sharma",
            "age": 22
        }
    ]
}

-------------------------------------------------

{
    "CSE": [
        {
            "Fullname": " Gudal Sharma",
            "age": 22
        }
    ],
    "IT": [
        {
            "Fullname": "Bhrati Kumari",
            "age": 23
        }
    ]
}

-------------------------------------------------

{
    "CSE": [
        {
            "Fullname": " Gudal Sharma",
            "age": 22
        },
        {
            "Fullname": "Niyati",
            "age": 22
        }
    ],
    "IT": [
        {
            "Fullname": "Bhrati Kumari",
            "age": 23
        }
    ]
}

-------------------------------------------------

{
    "CSE": [
        {
            "Fullname": " Gudal Sharma",
            "age": 22
        },
        {
            "Fullname": "Niyati",
            "age": 22
        }
    ],
    "IT": [
        {
            "Fullname": "Bhrati Kumari",
            "age": 23
        }
    ],
    "MECH": [
        {
            "Fullname": "Prince Harry",
            "age": 22
        }
    ]
}

-------------------------------------------------

{
    "CSE": [
        {
            "Fullname": " Gudal Sharma",
            "age": 22
        },
        {
            "Fullname": "Niyati",
            "age": 22
        }
    ],
    "IT": [
        {
            "Fullname": "Bhrati Kumari",
            "age": 23
        }
    ],
    "MECH": [
        {
            "Fullname": "Prince Harry",
            "age": 22
        }
    ],
    "CIVIL": [
        {
            "Fullname": "Final Singh",
            "age": 21
        }
    ]
}

-------------------------------------------------


C:\\Users\\Sjain Ventures\\Desktop\\Hira>
"""